<?php
namespace App\Suport;

use Carbon\Carbon;
use MongoDB\BSON\UTCDateTime;

class MongoTime{

    public static function nowTime() {

        $nowTime = new UTCDateTime(new \DateTime('now'));

        return $nowTime;
    }

    public static function makeTime($time) {

        $nowTime = new UTCDateTime($time);

        return $nowTime;
    }

    public static function localTime() {

        $localTime = Carbon::now()->addHour(7);

        $nowTime = new UTCDateTime($localTime);

        return $nowTime;
    }
}