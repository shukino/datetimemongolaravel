<?php

namespace App\Http\Controllers;

use App\Models\TrxList;
use App\Suport\MongoTime;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function insertTrx() {

        $arrayTrx = [
            "ref_id" => "ref_123",
            "trx_id" => 123,
            "username" => "andre",
            "nominal" => 20000,
            "created_at" => MongoTime::localTime(),
            "updated_at" => MongoTime::localTime(),
        ];

        TrxList::insert($arrayTrx);

        /* $dataTrx = new TrxList();
        $dataTrx->ref_id = "ref_456";
        $dataTrx->trx_id = 456;
        $dataTrx->username = "zukino";
        $dataTrx->nominal = 30000;
        $dataTrx->save(); */

        $responArray = [
            "code" => 0,
            "messaage" => "success",
            "data" => []
        ];

        return response()->json($responArray);
    }

    public function getTrx() {

        $nowTime = Carbon::now();

        $dataTrx = TrxList::where('created_at', '<', $nowTime)
            ->get();

        $responArray = [
            "code" => 0,
            "messaage" => "success",
            "data" => $dataTrx
        ];

        return response()->json($responArray);
    }

    public function printDate() {

        $minusTime = Carbon::now()->subHours(2);

        $date = MongoTime::makeTime($minusTime);

        $responArray = [
            "code" => 0,
            "messaage" => "success",
            "data" => $date
        ];

        return response()->json($responArray);
    }
}
